export default {
  // Determines if a move is legal
  //   topCard: top card of the current stack
  //   card: proposed next move
  isLegal: function(topCard, manualColor, card, player, playHistory= []) {
    if ((topCard.type.name === '+2' || topCard.type.name === 'wild+4') && playHistory.length !== 0 && playHistory[playHistory.length - 1].playerId !== player && playHistory[playHistory.length - 1].action !== 'draw') {
      return card.type.name === 'wild+4' || card.type.name === '+2';
    }
    else if(topCard.color === 'special') {
      return card.color === 'special' || card.color === manualColor;
    }
    else {
      return card.color === 'special' || topCard.type.name === card.type.name || topCard.color === card.color/* || topCard.type.cat === card.type.cat*/;
    }
  }
}
