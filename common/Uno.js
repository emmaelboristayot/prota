import DeckBuilder from './DeckBuilder';
import Players from './Players';
import Rules from './Rules';
import UnoState from './UnoState';
import {AudioPlayer} from './AudioPlayer'

export default class {
  constructor(playerConfig, emit) {
    const deck = DeckBuilder.createDeck(playerConfig.length);
    DeckBuilder.shuffleDeck(deck);
    const top = DeckBuilder.getTop(deck);
    deck.splice(top.index, 1);

    this.deck = deck;
    this.secondaryDeck = [];
    this.stack = [top.card];
    this.players = [...Players.createPlayers(playerConfig, deck)];
    this.playersInit = [...Players.createPlayers(playerConfig, deck)];
    this.boardDirection = +1;
    this.manualColor = null;
    this.currentPlayer = this.players[0].id;
    this.nextQueued = false;
    this.drawCount = 0;
    this.playHistory = []

    this.emit = emit;

    this.audioPlayer = new AudioPlayer();
  }

  get topStack() {
    return this.stack[0];
  }

  getPlayers() {
    return this.players;
  }

  get playerList() {
    return this.players.map(player => player.id);
  }

  get nextPlayer() {
    let nextIndex = this.playerList.indexOf(this.currentPlayer) + this.boardDirection;
    if(nextIndex >= this.playerList.length) nextIndex = 0;
    else if(nextIndex < 0)  nextIndex = this.playerList.length - 1;
    return this.playerList[nextIndex];
  }

  get previousPlayer() {
    let nextIndex = this.playerList.indexOf(this.currentPlayer) - this.boardDirection;
    if(nextIndex >= this.playerList.length) nextIndex = this.playerList.length - 1;
    else if(nextIndex < 0)  nextIndex = 0;
    return this.playerList[nextIndex];
  }

  getPlayer(playerId) {
    return this.players.filter(player => player.id === playerId)[0] || {};
  }

  playCard(playerId, card) {
    if(Rules.isLegal(this.topStack, this.manualColor, card, this.currentPlayer, this.playHistory)) {
      this.audioPlayer.playAudio();
      this.stack.unshift(card);
      this.secondaryDeck.push(card);

      const played = {
        player: this.getPlayer(playerId),
        action: 'play',
        cards: []
      };

      played.cards.push(card);
      this.playHistory.push(played);

      const spliceIndex = DeckBuilder.indexOf(this.getPlayer(playerId).hand, card);
      const hand = JSON.parse(JSON.stringify(this.getPlayer(playerId).hand))
      hand.splice(spliceIndex, 1);

      this.getPlayer(playerId).hand = hand;
      this.getPlayer(playerId).selectedCardIndex = -1;

      if(this.playSideEffects(playerId, card) === true) return;

      this.nextTurn();
    }
  }

  playSideEffects(playerId, card) {
    if(card.type.name === 'skip') {
      this.nextTurn(true);
    }
    if(card.type.name === 'reverse') {
      this.audioPlayer.reverseAudio();
      this.boardDirection *= -1;
      if(this.players.length === 2) {
        this.nextTurn(true);
      }
    }
    if(card.type.name === '+2') {
      this.audioPlayer.attackAudio();
      this.drawCount += 2

      if (this.getPlayer(this.nextPlayer).hand.filter(card => Rules.isLegal(this.topStack, this.manualColor, card, this.nextPlayer, this.playHistory)).length === 0) {
        this.emit('draw', {player: this.nextPlayer, number: this.drawCount});
        this.draw(this.nextPlayer, this.drawCount);
        this.nextTurn(true);
      }
    }
    if(card.type.name === 'wild' || card.type.name === 'wild+4') {
      this.manualColor = null;

      if(card.type.name === 'wild+4') {
        this.audioPlayer.attackAudio();
        this.drawCount += 4

        if (this.getPlayer(this.nextPlayer).hand.filter(card => Rules.isLegal(this.topStack, this.manualColor, card, this.nextPlayer, this.playHistory)).length === 0) {
          this.emit('draw', {player: this.nextPlayer, number: this.drawCount});
          this.draw(this.nextPlayer, this.drawCount);
          this.nextTurn(true, true); // TODO verify still works on multiplayer
        }
      }

      this.emit('needColor');

      return true;
    }
  }

  setManualColor(color) {
    this.manualColor = color;
    this.playHistory[this.playHistory.length - 1].manualColor = color
    this.nextTurn();
  }

  draw(playerId, n = 1) {
    const played = {
      player: this.getPlayer(playerId),
      action: 'draw',
      cards: []
    };

    if (n===1 && this.drawCount !==0) {
      n = this.drawCount
    }

    if(this.deck.length < n) {
      DeckBuilder.shuffleDeck(this.secondaryDeck);

      for(let i = 0; i < this.secondaryDeck.length; i++) {
        this.deck.unshift(this.secondaryDeck[i]);
      }

      this.secondaryDeck = [];
    }

    for(let i = 0; i < n; i++) {
      const drawIndex = Math.floor(Math.random() * this.deck.length);
      this.getPlayer(playerId).hand.push(this.deck[drawIndex]);
      this.deck.splice(drawIndex, 1);

      played.cards.push(this.deck[drawIndex]);
    }

    this.playHistory.push(played);

    this.getPlayer(playerId).hand.sort((a, b) => (a.color > b.color) ? 1 : -1);

    this.drawCount = 0
  }

  nextTurn(isEffect = false, delayUntilNextCall = false) {
    if(this.nextQueued) {
      this.nextQueued = false;
      this.nextTurn(true);
    }

    if(delayUntilNextCall) {
      this.nextQueued = true;
      return;
    }

    if(this.players.length === 1 || ( this.getPlayer(this.currentPlayer).hand !== undefined && this.getPlayer(this.currentPlayer).hand.length === 0 )) {
      this.emit('win');
      this.currentPlayer = null;
      return;
    }

    this.currentPlayer = this.nextPlayer;

    if(!isEffect) {
      this.emit('nextTurn');
    }
  }

  remoteSetState(unoState) {
    // UnoState.apply(this, unoState);
    this.boardDirection = unoState.boardDirection;
    this.currentPlayer = unoState.currentPlayer;
    this.deck = unoState.deck;
    this.manualColor = unoState.manualColor;
    this.players = [...unoState.players.map((player, i) => {
      return {
        hand: player.hand,
        human: player.human,
        id: player.id,
        name: player.name,
        selectedCardIndex: player.selectedCardIndex,
        remote: true
      }
    })];

    this.playersInit = [...unoState.players.map((player, i) => {
      return {
        hand: player.hand,
        human: player.human,
        id: player.id,
        name: player.name,
        selectedCardIndex: player.selectedCardIndex,
        remote: true
      }
    })];
    this.stack = [...unoState.stack];
    this.playHistory = [...unoState.playHistory];
  }

  remoteSetPlayerHand(id, hand) {
    this.getPlayer(id).hand = hand;
  }

  remoteSetPlayerHandLength(id, length) {
    this.getPlayer(id).hand = UnoState.generateHandWithLength(length);
  }

  remoteSetPlayerSelectedCardIndex(id, index)  {
    this.getPlayer(id).selectedCardIndex = index;
  }

  remoteSetCurrentPlayer(id) {
    this.currentId = id;
  }

  remoteSetBoardDirection(direction) {
    this.boardDirection = direction;
  }

  remoteSetManualColor(color) {
    this.manualColor = color;
  }

  remoteSetStack(stack) {
    this.stack = stack;
  }
}
