const prophets = [
  {name: 'Elisée', cat: 'P', size: 2},
  {name: 'Jonas', cat: 'P', size: 2},
  {name: 'Samuel', cat: 'P', size: 2},
  // {name: 'Jeremie', cat: 'P', size: 2},
  // {name: 'Daniel', cat: 'P', size: 2},
  // {name: 'Elie', cat: 'P', size: 2},
  // {name: 'Josué', cat: 'P', size: 2},
  // {name: 'Nahum', cat: 'P', size: 2},
  // {name: 'Zacharie', cat: 'P', size: 2},
  // {name: 'Aggée', cat: 'P', size: 2},
  // {name: 'Malachie', cat: 'P', size: 2},
  // {name: 'Sophonie', cat: 'P', size: 2},
  // {name: 'Habacuc', cat: 'P', size: 2},
  // {name: 'Michée', cat: 'P', size: 2},
  // {name: 'Abdias', cat: 'P', size: 2},
  // {name: 'Joel', cat: 'P', size: 2},
  // {name: 'Osée', cat: 'P', size: 2},
  // {name: 'Ezéchiel', cat: 'P', size: 2},
]

const servants = [
  // {name: 'Ruth', cat: 'S', size: 2},
  // {name: 'Esther', cat: 'S', size: 2},
  // {name: 'Noé', cat: 'S', size: 2},
  // {name: 'Trophime', cat: 'S', size: 2},
]

const kings = [
  // {name: 'Achab', cat: 'R', size: 2},
  {name: 'David', cat: 'R', size: 2},
  {name: 'Saul', cat: 'R', size: 2},
  {name: 'Salomon', cat: 'R', size: 2},
  // {name: 'Abimelec', cat: 'R', size: 2},
  // {name: 'Herod', cat: 'R', size: 2},
  // {name: 'Azarias', cat: 'R', size: 2},
  // {name: 'Achaz', cat: 'R', size: 2},
  // {name: 'Ezechias', cat: 'R', size: 2},
  // {name: 'Agrippa I', cat: 'R', size: 2},
  // {name: 'Jéroboam', cat: 'R', size: 2},
  // {name: 'Nadab', cat: 'R', size: 2},
  // {name: 'Amon', cat: 'R', size: 2},
]

const apostles = [
  {name: 'Pierre', cat: 'A', size: 2},
  // {name: 'André', cat: 'A', size: 2},
  {name: 'Jean', cat: 'A', size: 2},
  // {name: 'Barthélemy', cat: 'A', size: 2},
  {name: 'Paul', cat: 'A', size: 2},
  // {name: 'Thomas', cat: 'A', size: 2},
  // {name: 'Jude', cat: 'A', size: 2},
  // {name: 'Matthias', cat: 'A', size: 2},
  // {name: 'Simon', cat: 'A', size: 2},
  // {name: 'Jacques', cat: 'A', size: 2},
  // {name: 'Philippe', cat: 'A', size: 2},
  // {name: 'Matthieu', cat: 'A', size: 2},
  // {name: 'Barnabas', cat: 'A', size: 2},
]

const specialColor = [
  {name: 'skip', cat: 1, size: 2},
  {name: 'reverse', cat: 2, size: 2},
  {name: '+2', cat: 3, size: 2},
]

const special = [
  {name: 'wild', cat: 4, size: 4},
  {name: 'wild+4', cat: 5, size: 4}
]

export default {
  validColors: ['red', 'yellow', 'green', 'blue', 'special'],
  validTypes: [].concat(prophets, servants, kings, apostles, specialColor, special),
  deckConfig: {
    colors: ['red', 'yellow', 'green', 'blue'],
  },
  handConfig: {
    initialCount: 7
  },
  topConfig: {
    validColors: ['red', 'yellow', 'green', 'blue'],
    validTypes: [].concat(prophets, servants, kings, apostles)
  },

  // Creates a card with a given color and type
  createCard: function(color, type) {
    if(this.validColors.indexOf(color) === -1) {
      throw 'Invalid card color ' + color;
    }

    if(this.validTypes.map(function(e) { return e.name; }).indexOf(type) === -1) {
      throw 'Invalid card type ' + type;
    }

    const validType = this.validTypes.filter(validType => validType.name === type)[0];

    return {
      color: color,
      type: validType
    }
  },

  // Creates a deck of cards according to the rules in deckConfig
  createDeck: function(numPlayers = 1) {
    const res = [];

    this.deckConfig.colors.forEach(color => {
      [].concat(prophets, servants, kings, apostles, specialColor).forEach(type => {
        for(let i = 0; i < type.size; i++) {
          res.push(this.createCard(color, type.name));
        }
      });
    });

    special.forEach(type => {
      for(let i = 0; i < type.size; i++) {
        res.push(this.createCard('special', type.name));
      }
    });

    if(numPlayers > 10) {
      res.push(...this.createDeck(numPlayers - 10));
    }

    return res;
  },

  // Returns a hand, subtracting cards from a given deck
  createHand: function(deck) {
    const hand = [];
    for(let i = 0; i < this.handConfig.initialCount; i++) {
      const j = Math.floor(Math.random() * deck.length);
      hand.push(deck[j]);
      deck.splice(j, 1);
    }
    hand.sort((a, b) => (a.color > b.color) ? 1 : -1)
    return hand;
  },

  // Returns a valid card to go on top of the stack
  getTop: function(deck) {
    const card = deck.filter(card => this.topConfig.validColors.indexOf(card.color) > -1 && this.validTypes.map(function(e) { return e.name; }).indexOf(card.type.name) > -1)[0];
    return {
      index: deck.indexOf(card),
      card: card
    }
  },

  // Durstenfeld shuffle: https://stackoverflow.com/a/12646864/1110858
  shuffleDeck: function(deck) {
    for (let i = deck.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [deck[i], deck[j]] = [deck[j], deck[i]];
    }
  },

  equals(a, b) {
    return a && b && a.color === b.color && a.type.name === b.type.name/* && a.type.cat === b.type.cat*/;
  },

  indexOf(hand, card) {
    for(let i = 0; i < hand.length; i++) {
      if(this.equals(hand[i], card)) {
        return i;
      }
    }

    return -1;
  }
}
