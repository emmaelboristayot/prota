import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Lobby from '../views/Lobby.vue'
import Game from '../views/Game.vue'
import Profile from '../views/Profile.vue'
import ChangeAvatar from '../views/ChangeAvatar.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/lobby'
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
  },
  {
    path: '/avatar',
    name: 'avatar',
    component: ChangeAvatar,
  },
  {
    path: '/lobby',
    name: 'Lobby',
    component: Lobby,
  },
  {
    path: '/lobby/:id',
    name: 'In-Match Lobby',
    props: true,
    component: Lobby,
  },
  {
    path: '/join/:id',
    redirect: '/lobby/:id'
  },
  {
    path: '/game/',
    name: 'Game',
    component: Game
  },
  {
    path: '/game/offline',
    name: 'Offline Game',
    component: Game
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
