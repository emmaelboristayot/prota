import { createApp } from 'vue'
import App from './App.vue'
import router from './router';

import { IonicVue } from '@ionic/vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

import network from './network';
import { createI18n } from 'vue-i18n'
import messages from './locales/en.json'
import VueSocketIO from "vue-socket.io";

import toast from "@/toast";

// call with I18n option
const i18n = createI18n({
  locale: 'en',
  messages: { en: messages }
})

const app = createApp(App)
    .use(IonicVue)
    .use(router)
    .use(i18n)
    .use(toast)
    .use(network, {local: false})
;

export function setI18nLanguage(i18n: any, locale: string) {
  if (i18n.mode === 'legacy') {
    i18n.global.locale = locale
  } else {
    i18n.global.locale.value = locale
  }
}

export function setupI18n(options = { locale: 'en' }) {
  const i18n = createI18n(options)
  setI18nLanguage(i18n, options.locale)
  return i18n
}

export async function loadLocaleMessages(i18n: any, locale: string) {
  // load locale messages
  if (!i18n.global.availableLocales.includes(locale)) {
    const messages = await import(
        /* webpackChunkName: "locale-[request]" */ `./locales/${locale}.json`
        )
    i18n.global.setLocaleMessage(locale, messages.default)
  }
}
  
router.isReady().then(() => {
  app.mount('#app');
});