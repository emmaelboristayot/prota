import Toasted from 'vue-toasted';

const toast = {
  install(Vue: any, options: any) {
    console.log(Vue)
    Vue.prototype = Vue.config.globalProperties
    Vue.use(Toasted);
  }
}

export default toast