function computeFraction(i, total) {
  if(total === 1) {
    return 1 / 2;
  }
  else if(total === 2) {
    // return (i + 1) / 3;
    return i / (total - 1);
  }
  else if(total !== 2) {
    return i / (total - 1); // subtract 1 because arrays are 0-indexed while math is 1-indexed
  }
}

function computeFractionFourFive(i, total) {
  if(total === 4) {
    const index = (i===1) ? 0 : (i===2) ? i+1 : i

    return index / (total - 1); // subtract 1 because arrays are 0-indexed while math is 1-indexed
  }
  if(total === 5 ) {
    const index = (i===1) ? 0 : (i===3) ? i+1 : i

    return index / (total - 1); // subtract 1 because arrays are 0-indexed while math is 1-indexed
  }
}

export default {
  // Get position of opponent detail box along the radius of a semi-circle given:
  //   i: the index of current opponent
  //   total: total number of enemy players to place
  getOpponentDetailStyle(i, total) {
    const frac =  computeFraction(i, total);
    const radiansFourFive = computeFractionFourFive(i, total) * Math.PI;
    const radians = frac * Math.PI;
    const topFourFive = 'calc(40vh * ' + -1 * Math.sin(radians) + ' + 50vh)';


    return {
      left: 'calc(40% * ' + -1 * Math.cos((total === 4 || total === 5 ) ? radiansFourFive : radians) + ' + 50%)',
      top: (total > 3) ? topFourFive : 'calc(17vh * ' + -1 * Math.sin(radians) + ' + 25vh)',
      // transform: 'translateX(-' + (frac * 150) + 'px)'
      transform: 'translate(-50%,-50%)'
    }
  },

  // Get position and rotation of opponent hand along the radius of a semi-circle given:
  //   i: the index of current opponent
  //   total: total number of enemy players to place
  getOpponentHandStyle(i, total) {
    const frac =  computeFraction(i, total);
    const radians = frac * Math.PI;
    const radiansFourFive = computeFractionFourFive(i, total) * Math.PI;

    return {
      left: 'calc(50% * ' + -1 * Math.cos(((total === 4 || total === 5 ) ? radiansFourFive : radians)) + ' + 50% + ' + (-100 * (((total === 4 || total === 5 ) ? computeFractionFourFive(i, total) : frac) - 0.5))  + 'px)',
      top: 'calc(55vh * ' + -1 * Math.sin(radians) + ' + 55vh + 50px)',
      transform: 'rotate(' + (0.5 * Math.PI + ((total === 4 || total === 5 ) ? radiansFourFive : radians)) + 'rad) translateX(-' + ((1 - frac) * 100) + '%)'
    }
  }
}
