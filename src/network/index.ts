import VueSocketIO from 'vue-socket.io'

const network = {
  install(Vue: any, options: any) {
    Vue.prototype = Vue.config.globalProperties
    let online = true

    let connectionUrl = 'http://localhost:3000';

    if (!options.local) {
      connectionUrl = 'https://christian-uno.ew.r.appspot.com'
    }

    Vue.use(new VueSocketIO({
          debug: true,
          connection: connectionUrl, //options object is Optional
        })
    );

    Vue.config.globalProperties.$network = {
      get baseUrl() {
        return !options.local ?
            'https://playuno.app' :
            'http://localhost:8100';
      },

      // get offline(): any  {
      //   return Vue.config.globalProperties.$router.currentRoute.value.path.indexOf('offline') > -1;
      // },

      get online(): boolean  {
        // return Vue.config.globalProperties.$router.currentRoute.value.path.indexOf('offline') === -1;
        return online
      },

      set online(payload) {
        online = payload
      },

      emit(eventName: any, payload?: any) {
        if (!this.offline) {
          Vue.prototype.$socket.emit(eventName, payload);
        } else {
          console.log('Attempted to emit an event while offline: ' + eventName);
        }
      },

    };
  }
}

export default network