import Connection from './models/Connection.js';
import Match from './models/Match.js';

import Chat from './modules/Chat.js';
import Matchmaking from './modules/Matchmaking.js';
import Multiplayer from './modules/Multiplayer.js';

var app = require('express')()
var http = require('http').Server(app)
const io = require('socket.io')(http)

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);

  next();
})

io.on('connection', function(socket) {
  const connection = new Connection(socket);
  console.log('a user connected');

  socket.emit('refreshMatches', Match.getOpenMatches());

  socket.on('disconnect', () => {
    console.log('a user disconnected');
    connection.disconnect();
    if(connection.inMatch()) {
      connection.getMatch().removePlayer(connection.getId());
    }
  });

  Chat.apply(socket, connection);
  Matchmaking.apply(socket, connection);
  Multiplayer.apply(socket, connection);
});

app.get('/', (req, res) => {
  res.send(Object.keys(io.sockets.clients().connected))
})

http.listen(3000, () => {
  console.log('Listening on *:3000')
})
